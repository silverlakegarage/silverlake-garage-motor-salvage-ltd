Known locally as Silverlake Automotive Recycling, Silverlake Garage (Motor Salvage) Ltd are well known in the local area for providing scrap car collections, cheap car parts, online salvage auctions, salvage repairables and cheap car tyres.

Address: Row Ash, Botley Road, Shedfield, Southampton, Hampshire SO32 2HL, UK

Phone: +44 23 8022 9999

Website: https://www.silverlake.co.uk
